package javadoc;

import java.util.List;
import java.util.Set;

/**
 * This class inherits the parameter input methods basing on the School Interface.
 */

public class SDAcademySchool implements School {
    @Override
    public void addStudent(Student student) {}
    @Override
    public void addStudent(String firstName, String lastName, int age, Set<Course> courses) {}
    @Override
    public Student getStudentByFirstNameAndLastName(String firstName, String lastName) {
        return null;
    }

    /**
     * Returns a list of students sorted by the course type
     * @param course
     * @return
     */
    @Override
    public List<Student> getStudentsByCourse(Course course) {
        return null;
    }
}