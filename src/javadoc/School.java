package javadoc;

import java.util.List;
import java.util.Set;

/**
 * This is an interface class which enables us to create students.
 */

public interface School {

    /**
     * Adds student given in a parameter to the school.
     * @param student - Student added to school.
     */
    void addStudent(Student student);

    /**
     * Creates and adds a student to the school. Method requires all parameters to create a single student.
     * @param firstName - first name of the student that is going to be created.
     * @param lastName - second name of the student that is going to be created.
     * @param age - age of the student that is going to be created.
     * @param courses - courses which student created attends.
     */
    void addStudent(String firstName, String lastName, int age, Set<Course> courses);
    Student getStudentByFirstNameAndLastName(String firstName, String lastName);
    List<Student> getStudentsByCourse(Course course);
}
