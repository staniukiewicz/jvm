package javadoc;

/**
 * This Enum represents every type of Course.
 */
public enum Course {

    /**
     * This value represents backend developer course.
     */

    BACKEND_DEVELOPER("Backend Developer"),

    /**
     * This value represents a frontend developer in technologies such as Angular or React.
     */
    FRONTEND_DEVELOPER("Frontend Developer"),

    /**
     *  This value represent a person who deals with something which is meant to be autonomous but in fact - is not.
     */
    AI_ENGINEER("AI Engineer"),

    /**
     *  Devil operations.
     */
    DEV_OPS("Dev ops");

    private String courseName;

    /**
     * Course contructor, creates a course with decsrption given in parameter.
     * @param courseName - course description/ name.
     */
    Course(String courseName) {
        this.courseName = courseName;
    }

    /**
     * Returns the course name.
     * @return
     */

    public String getCourseName() {
        return courseName;
    }
}

