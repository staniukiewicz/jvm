package visualvm;

public class Ticket {
    private int id;

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                '}';
    }

    public Ticket(int id) {
        this.id = id;
    }
}
